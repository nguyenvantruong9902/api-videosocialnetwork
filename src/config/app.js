import { express } from "express";
import { bodyParser } from "body-parser";
import { cors } from "cors";

class App {

    constructor() {
        this.app = express();
    }

    appConfig() {
        this.app.use(cors());
        this.app.use(bodyParser.json());
        this.app.listen(3000);
    }

    includeRoutes() {

    }

    execute() {
        this.appConfig();
        this.includeRoutes();
    }
}

module.exports = new App(); 